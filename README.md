# Infrastructure Setup with Terraform

This repository contains Terraform configuration files to provision a web application infrastructure on AWS. The infrastructure includes a VPC with public and private subnets, an EC2 instance, an RDS PostgreSQL database, security groups, an IAM role with an S3 read policy, an API Gateway with a Lambda function that interacts with the RDS PostgreSQL database, and a Route 53 hosted zone.

## Directory Structure

project-root/
├── main.tf
├── variables.tf
├── outputs.tf
├── terraform.tfvars
├── vpc/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf
├── ec2/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf
├── rds/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf
├── security-groups/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf
├── route53/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf
├── s3/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf
├── lambda/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf
│ ├── lambda_function.py
│ └── requirements.txt
├── api-gateway/
│ ├── main.tf
│ ├── variables.tf
│ ├── outputs.tf

markdown
Copy code

## Prerequisites

- [Terraform](https://www.terraform.io/downloads.html) installed
- AWS account with necessary permissions
- An S3 bucket for Terraform state (optional, for remote state management)

## Setup Instructions

### 1. Initialize Terraform

First, you need to initialize the Terraform working directory.

```sh
terraform init
2. Create terraform.tfvars File
Create a terraform.tfvars file in the project-root directory with the following contents:

hcl
Copy code
db_password = "12345678"
ec2_instance_profile_name = "omar-ec2-instance-profile"
ec2_role_name = "omartest"
s3_bucket_name = "omarkhalafbucket"
s3_read_policy_name = "s3p"
domain_name = "omarvoice.com"
db_name = "khalafomar"
db_username = "omarkhalaf98"
Ensure that all values are filled in correctly, especially the s3_bucket_name, which must be globally unique.

3. Plan the Infrastructure
Run the following command to see what changes Terraform will make to your infrastructure.

sh
Copy code
terraform plan
4. Apply the Configuration
Apply the Terraform configuration to create the infrastructure.

sh
Copy code
terraform apply
You will be prompted to enter the value for var.domain_name. Enter your domain name (e.g., omarvoice.com).

5. Access the Infrastructure
After the terraform apply command completes successfully, you can access the API Gateway URL. The URL will be displayed as an output of the Terraform apply command.

Tearing Down the Infrastructure
To destroy all the resources created by Terraform, run the following command:

sh
Copy code
terraform destroy
You will be prompted to confirm the destruction of the infrastructure. Type yes to proceed.

Notes
Ensure that your AWS credentials are configured properly. You can configure your credentials using the AWS CLI:

sh
Copy code
aws configure
Review and customize the Terraform configuration files as needed for your specific use case.

Make sure to replace any placeholder values with your actual values, such as ami-04fe22dfadec6f0b6 with a valid AMI ID for your region.