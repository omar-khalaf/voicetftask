import psycopg2
import os

def lambda_handler(event, context):
    db_host = os.environ['DB_HOST']
    db_name = os.environ['DB_NAME']
    db_user = os.environ['DB_USER']
    db_password = os.environ['DB_PASSWORD']
    
    try:
        connection = psycopg2.connect(
            host=db_host,
            database=db_name,
            user=db_user,
            password=db_password
        )
        
        cursor = connection.cursor()
        cursor.execute("SELECT version();")
        version = cursor.fetchone()
        
        cursor.close()
        connection.close()
        
        return {
            'statusCode': 200,
            'body': f'DB version: {version}'
        }
        
    except Exception as e:
        return {
            'statusCode': 500,
            'body': str(e)
        }
