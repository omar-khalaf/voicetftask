provider "aws" {
  region = "eu-west-1"
}

module "vpc" {
  source = "./vpc"
  vpc_cidr_block = "10.0.0.0/16"
  public_subnet_cidr = ["10.0.1.0/24", "10.0.3.0/24"]
  private_subnet_cidr = ["10.0.2.0/24", "10.0.4.0/24"]
  vpc_name = "my-vpc"
}

module "security_groups" {
  source = "./security-groups"
  vpc_id = module.vpc.vpc_id
  vpc_name = "my-vpc"
}

module "iam" {
  source = "./iam"
  ec2_role_name = var.ec2_role_name
  s3_read_policy_name = var.s3_read_policy_name
  s3_bucket_name = var.s3_bucket_name
  ec2_instance_profile_name = var.ec2_instance_profile_name
}

module "ec2" {
  source = "./ec2"
  ami_id = "ami-04fe22dfadec6f0b6"  
  instance_type = "t2.micro"
  subnet_id = module.vpc.private_subnet_id[0]
  security_group_ids = [module.security_groups.web_security_group_id]
  instance_name = "my-private-instance"
  iam_instance_profile = var.ec2_instance_profile_name
}

module "rds" {
  source = "./rds"
  subnet_id_1 = module.vpc.private_subnet_id[0]
  subnet_id_2 = module.vpc.private_subnet_id[1]
  security_group_ids = [module.security_groups.rds_security_group_id]
  db_instance_identifier = "omarkhalaf"
  db_name = "khalafomar"
  db_username = "omarkhalaf98"
  db_password = var.db_password
}

module "route53" {
  source = "./route53"
  domain_name = var.domain_name  
  ec2_public_dns = module.ec2.instance_public_dns
  ec2_zone_id = module.route53.zone_id 
}

module "s3" {
  source = "./s3"
  bucket_name = var.s3_bucket_name
}

module "lambda" {
  source = "./lambda"
  db_name = var.db_name
  db_username = var.db_username
  db_password = var.db_password
}

module "api_gateway" {
  source = "./api-gateway"
}
