output "web_security_group_id" {
  description = "The ID of the web security group"
  value       = aws_security_group.web_sg.id
}

output "rds_security_group_id" {
  description = "The ID of the RDS security group"
  value       = aws_security_group.rds_sg.id
}
