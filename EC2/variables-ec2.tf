variable "ami_id" {
  description = "The AMI ID to use for the instance"
  type        = string
} 

variable "instance_type" {
  description = "The instance type to use"
  type        = string
  default     = "t2.micro"
}

variable "subnet_id" {
  description = "The ID of the subnet"
  type        = string
}

variable "security_group_ids" {
  description = "List of security group IDs"
  type        = list(string)
}

variable "instance_name" {
  description = "Name tag for the instance"
  type        = string
  default     = "my-instance"
}

variable "iam_instance_profile" {
  description = "The IAM instance profile to attach to the instance"
  type        = string
}
