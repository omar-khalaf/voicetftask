variable "lambda_function_name" {
  description = "The name of the Lambda function to integrate with the API Gateway"
  type        = string
}
