variable "domain_name" {
  description = "The domain name to create the hosted zone for"
  type        = string
}

variable "ec2_public_dns" {
  description = "The public DNS name of the EC2 instance"
  type        = string
}

variable "ec2_zone_id" {
  description = "The zone ID of the EC2 instance"
  type        = string
}
