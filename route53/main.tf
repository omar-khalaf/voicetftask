resource "aws_route53_zone" "this" {
  name = var.domain_name
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.this.zone_id
  name    = "www.${var.domain_name}"
  type    = "CNAME"  # Change from A to CNAME
  ttl     = 300
  records = [var.ec2_public_dns]
}
