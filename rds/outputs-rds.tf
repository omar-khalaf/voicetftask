output "endpoint" {
  description = "The endpoint of the RDS instance"
  value       = aws_db_instance.postgres.endpoint
}

output "username" {
  description = "The master username of the RDS instance"
  value       = aws_db_instance.postgres.username
}
