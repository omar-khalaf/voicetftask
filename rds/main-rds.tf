resource "aws_db_subnet_group" "main" {
  name       = "main"
  subnet_ids = [var.subnet_id_1, var.subnet_id_2]

  tags = {
    Name = "main"
  }
}

resource "aws_db_instance" "postgres" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "13.14"  # Replace with a valid version
  instance_class       = "db.t3.micro"
  identifier           = var.db_instance_identifier
  db_name              = var.db_name
  username             = var.db_username  # Update this to a non-reserved username
  password             = var.db_password
  db_subnet_group_name = aws_db_subnet_group.main.name
  vpc_security_group_ids = var.security_group_ids
  skip_final_snapshot  = true

  tags = {
    Name = var.db_instance_identifier
  }

  storage_encrypted = true
}
