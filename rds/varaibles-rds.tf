variable "subnet_id_1" {
  description = "The ID of the first subnet"
  type        = string
}

variable "subnet_id_2" {
  description = "The ID of the second subnet"
  type        = string
}

variable "security_group_ids" {
  description = "List of security group IDs"
  type        = list(string)
}

variable "db_instance_identifier" {
  description = "The identifier for the RDS instance"
  type        = string
}

variable "db_name" {
  description = "The name of the database"
  type        = string
}

variable "db_username" {
  description = "The master username for the database"
  type        = string
    default     = "masteruser"  # Replace with a different non-reserved username

}

variable "db_password" {
  description = "Password for the RDS PostgreSQL database"
  type        = string
  sensitive   = true
}
