variable "db_password" {
  description = "Password for the RDS PostgreSQL database"
  type        = string
  sensitive   = true
}

variable "ec2_role_name" {
  description = "The name of the EC2 role"
  type        = string
}

variable "s3_read_policy_name" {
  description = "The name of the S3 read policy"
  type        = string
}

variable "s3_bucket_name" {
  description = "The name of the S3 bucket"
  type        = string
}

variable "ec2_instance_profile_name" {
  description = "The name of the EC2 instance profile"
  type        = string
}

variable "domain_name" {
  description = "The domain name to create the hosted zone for"
  type        = string
}

variable "db_name" {
  description = "The name of the database"
  type        = string
}

variable "db_username" {
  description = "The username for the database"
  type        = string
}
