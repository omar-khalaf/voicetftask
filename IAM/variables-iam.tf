variable "ec2_role_name" {
  description = "The name of the EC2 role"
  type        = string
}

variable "s3_read_policy_name" {
  description = "The name of the S3 read policy"
  type        = string
}

variable "s3_bucket_name" {
  description = "The name of the S3 bucket"
  type        = string
}

variable "ec2_instance_profile_name" {
  description = "The name of the EC2 instance profile"
  type        = string
}

